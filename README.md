# Modèle de microsimulation Autonomix Domicile 

Ce dossier fournit les programmes permettant de former le package contenant le modèle de microsimulation Autonomix version 1 de décembre 2021.

Lien vers la documentation du modèle : https://drees_code.gitlab.io/public/modeles/autonomix/DocumentationATNMX/

Présentation de la Drees : La Direction de la recherche, des études, de l'évaluation et des statistiques (DREES) est le service statistique ministériel des ministères sanitaires et sociaux, et une direction de l'administration centrale de ces ministères. 
https://drees.solidarites-sante.gouv.fr/article/presentation-de-la-drees

Source de données : Ce package utilise les données de la base CARE Ménage 2015, ou des RI 2017 ou des RI 2017 anonymisées dans leur version du 23 décembre 2021. Traitements : Drees.

Date de la dernière exécution des programmes avant publication, et version des logiciels utilisés : Les programmes ont été exécutés le 14/12/2021 avec la version 4.0.5 de R. Les packages mobilisés sont listés dans le fichier DESCRIPTION du package. 
