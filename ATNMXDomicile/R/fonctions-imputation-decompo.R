# Copyright (C) 2021-2022. Logiciel élaboré par l'État, via la Drees.

# Pole Simulation du bureau Handicap et Dépendance, Drees.

# Ce programme informatique a été développé par la Drees. Il permet d'exécuter le modèle de microsimulation Autonomix version 1.0.1 de juillet 2022.

# Ce programme a été exécuté le 19/07/2022 avec la version 4.0.3 de R et les packages mobilisés sont listés dans le fichier DESCRIPTION du projet »

# La documentation du modèle peut être consultés sur [gitlab](https://drees_code.gitlab.io/public/modeles/autonomix/DocumentationATNMX/)

# Ce programme utilise les données de la base CARE Ménage 2015, ou des RI 2017 ou des RI 2017 anonymisées dans leur version du 23 décembre 2021

# Bien qu'il n'existe aucune obligation légale à ce sujet, les utilisateurs de ce programme sont invités à signaler à la DREES leurs travaux issus de la réutilisation de ce code, ainsi que les éventuels problèmes ou anomalies qu'ils y rencontreraient, en écrivant à DREES-CODE@sante.gouv.fr

# Ce logiciel est régi par la licence EUPL V1.2 soumise au droit français et européen, respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence EUPL V1.2, telle que diffusée sur https://eupl.eu/1.2/fr/ et reproduite dans le fichier LICENCE diffusé conjointement au présent programme.
# En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.
# À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.


imput_ap_gir <- function(dt_, gir_){

  #' imput_ap_gir
  #'
  #' Impute le montant médian de l'aide ponctuelle aux bénéficiaires d'aide ponctuelle qui ont un montant manquant
  #' pour ce type d'aide, à GIR égal
  #'
  #' @param dt_, data.table contenant les données à traiter
  #' @param gir_, numeric GIR considéré
  #'
  #' @importFrom stats median
  #' @importFrom data.table :=
  #'
  #' @export
  #'

  dt_[AIDEPONC==1 & gir_imput == gir_ & AIDEPONC_NOT > plan_noti_2017*12, AIDEPONC_NOT := NA]
  medgir <- median(dt_[AIDEPONC==1 & gir_imput == gir_]$AIDEPONC_NOT, na.rm = TRUE)
  dt_[AIDEPONC==1 & gir_imput == gir_ & is.na(AIDEPONC_NOT), AIDEPONC_NOT := pmin(medgir, plan_noti_2017*12)]
}

imput_ah_gir <- function(dt_, gir_){

  #' imput_ah_gir
  #'
  #' Impute le montant médian de l'aide humaine aux bénéficiaires d'aide humaine qui ont un montant manquant
  #' pour ce type d'aide, à GIR égal
  #'
  #' @param dt_, data.table contenant les données à traiter
  #' @param gir_, numeric GIR considéré
  #'
  #' @importFrom stats median
  #' @importFrom data.table :=
  #'
  #' @export
  #'

  dt_[AIDEHUM==1 & gir_imput == gir_ & AIDEHUM_NOT > plan_noti_2017, AIDEHUM_NOT := NA]
  medgir <- median(dt_[AIDEHUM==1 & gir_imput == gir_]$AIDEHUM_NOT, na.rm = TRUE)
  dt_[AIDEHUM==1 & gir_imput == gir_ & is.na(AIDEHUM_NOT), AIDEHUM_NOT := pmin(medgir, plan_noti_2017)]
}

imput_aa_gir <- function(dt_, gir_){

  #' imput_aa_gir
  #'
  #' Impute le montant médian des autres aides mensuelles aux bénéficiaires d'autres aides mensuelles qui ont un montant manquant
  #' pour ce type d'aide, à GIR égal
  #'
  #' @param dt_, data.table contenant les données à traiter
  #' @param gir_, numeric GIR considéré
  #'
  #' @importFrom stats median
  #' @importFrom data.table :=
  #'
  #' @export
  #'

  dt_[AUTREAIDE==1 & gir_imput == gir_ & AUTREAIDE_NOT > plan_noti_2017, AUTREAIDE_NOT := NA]
  medgir <- median(dt_[AUTREAIDE==1 & gir_imput == gir_]$AUTREAIDE_NOT, na.rm = TRUE)
  dt_[AUTREAIDE==1 & gir_imput == gir_ & is.na(AUTREAIDE_NOT), AUTREAIDE_NOT := pmin(medgir, plan_noti_2017)]
}


composition_plan <- function(dt_){

  #' composition_plan
  #'
  #' Filtre pour conserver les départements qui ont remonté des informations de suffisamment bonne
  #' qualité pour étudier le montant des différents types d'aides.
  #' Fonction qui corrige les indicatrices de composition du plan (Aide humaine, aide ponctuelle, autres aides mensuelles)
  #' Les indicatrices manquantes sont mises à la modalité 0 (non bénéficiaire)
  #' Corrige aussi les montants des différents types d'aide : aide humaine, aide ponctuelle et autres aides mensuelles avec :
  #' - borne haute au plafond total des plans du GIR correspondant : individus au delà mis à NA
  #' - valeurs manquantes imputées à l'aide de la médiane
  #' Imputation d'un montant aide ponctuelle logement : pour les bénéficiaires d'aide ponctuelle logement on considère que tout le montant d'aide ponctuel du plan est dévolu à ce type d'aide
  #' Création indicatrice bénéficiaire d'accueil temporaire
  #' Si un seul type d'aide, alors le montant d'aide correspond au plan total 2017
  #' Correction des indicatrices de type d'emploi pour l'aide humaine : si le bénéficiaire n'a pas d'aide humaine, alors les
  #' indicatrices de "aide humaine en mode prestataire", "aide humaine en mode mandataire" et "aide humaine en gré à gré" sont mises à 0
  #'
  #' @param dt_ data.table, contient les données à modifier
  #'
  #' @importFrom stats median
  #' @importFrom data.table :=
  #'
  #' @export
  #'

  dep_to_keep <- c( "001", "003" ,"004", "005" ,"006" ,"007" ,"008", "010" ,
                    "011" ,"012", "015", "016", "018", "024",
                    "025", "026", "027", "02A", "030" ,"031", "032", "033",
                    "036", "037", "038", "039", "041", "042",
                    "043", "044", "045", "046", "048", "049", "050", "052",
                    "053", "054", "055", "056", "058", "060", "061",
                    "067", "068", "070", "071", "072", "073", "075", "076",
                    "077", "078", "079", "080", "083", "084",
                    "085", "086", "087", "088", "089", "090", "092", "093",
                    "094", "095", "971", "972", "974")

  # d'assez bonne qualité pour les analyses PAD cd note 23 2021
  dt_ <- dt_[DEP %in% dep_to_keep]

  # Création indicatrice bénéficiaire d'accueil temporaire
  dt_[, AIDEACCTEMP := 0+1*(AIDEACC==1 | AIDEHEB == 1)]

  # Imputation de toutes les indicatrices de type de plan manquant a 0
  dt_[is.na(AIDEHUM) | AIDEHUM==9, AIDEHUM := 0]
  dt_[is.na(AUTREAIDE)| AUTREAIDE==9, AUTREAIDE := 0]
  dt_[is.na(AIDEPONC)| AIDEPONC==9, AIDEPONC := 0]
  # Correction AIDEACC : si manquant alors non bénéficiaire de ce type d'aide
  dt_[is.na(AIDEACC) | AIDEACC==9, AIDEACC := 0]
  dt_[is.na(AIDEHEB) | AIDEHEB==9, AIDEHEB := 0]
  dt_[is.na(AIDEACCTEMP) | AIDEACCTEMP==9, AIDEACCTEMP := 0]

  # Imputation d'un montant aide ponctuelle logement ####
  dt_[is.na(AIDEPONC_LOG), AIDEPONC_LOG := 0] # Imputation des indicatrices manquantes a 0
  # Pour les bénéficiaires d'aide ponctuelle logement on considère que tout
  # le montant d'aide ponctuel du plan est dévolu a ce type d'aide
  imp_aidlog <- median(dt_[AIDEPONC_LOG==1&AIDEPONC_NOT>0]$AIDEPONC_NOT)
  dt_[AIDEPONC_LOG == 1 & is.na(AIDEPONC_NOT), AIDEPONC_NOT := imp_aidlog]

  summary(dt_[AIDEPONC_LOG==1]$AIDEPONC_NOT)

  # Montant notifié en aide humaine ####
  # si indicatrice à 0, alors montant à 0
  dt_[AIDEHUM == 0 & is.na(AIDEHUM_NOT), AIDEHUM_NOT := 0]

  imput_ah_gir(dt_, 1)
  imput_ah_gir(dt_, 2)
  imput_ah_gir(dt_, 3)
  imput_ah_gir(dt_, 4)


  summary(dt_[AIDEHUM==1]$AIDEHUM_NOT) # pas trop mal

  # Montant notifié en autre aides mensualisées ####
  summary(dt_[AUTREAIDE ==1]$AUTREAIDE_NOT) #9306 manquants
  # Imputation : pmin(médiane par GIR, montant de l'APA)
  dt_[AUTREAIDE == 0 & is.na(AUTREAIDE_NOT), AUTREAIDE_NOT := 0] #9306 manquants



  imput_aa_gir(dt_, 1)
  imput_aa_gir(dt_, 2)
  imput_aa_gir(dt_, 3)
  imput_aa_gir(dt_, 4)



  # Montant notifié en aide ponctuelle ####
  summary(dt_[AIDEPONC ==1]$AIDEPONC_NOT) #597 manquants
  # Imputation : pmin(médiane par GIR, montant de l'APA)
  dt_[AIDEPONC == 0 & is.na(AIDEPONC_NOT), AIDEPONC_NOT := 0]


  imput_ap_gir(dt_, 1)
  imput_ap_gir(dt_, 2)
  imput_ap_gir(dt_, 3)
  imput_ap_gir(dt_, 4)



  print(summary(dt_$AIDEHUM))
  print(summary(dt_$AUTREAIDE))
  print(summary(dt_$AIDEPONC))

  print("Montant aide humaine")
  print(summary(dt_[AIDEHUM==1]$AIDEHUM_NOT)) # 0 manquant
  print("Nb heures aide humaine")
  print(summary(dt_[AIDEHUM==1]$AIDEHUM_HEURE)) # 29870 manquant
  print("Montant aide ponctuelle")
  print(summary(dt_[AIDEPONC ==1]$AIDEPONC_NOT)) # 0 manquant
  print("Montant autre aide")
  print(summary(dt_[AUTREAIDE ==1]$AUTREAIDE_NOT)) # 0 manquant

  # Si un seul type d'aide, alors le montant d'aide correspond au plan total 2017
  dt_[AUTREAIDE == 0 & AIDEPONC == 0 & AIDEHUM == 1 & !is.na(plan_noti_2017), AIDEHUM_NOT := plan_noti_2017]
  dt_[AUTREAIDE == 0 & AIDEHUM == 0 & AIDEPONC == 1  & !is.na(plan_noti_2017), AIDEPONC_NOT:= plan_noti_2017]
  dt_[AIDEHUM == 0 & AIDEPONC == 0 & AUTREAIDE == 1  & !is.na(plan_noti_2017), AUTREAIDE_NOT := plan_noti_2017]

  # correction des indicatrices de type d'emploi
  dt_[AIDEHUM == 0, `:=`(AIDEHUM_PRESTA = 0, AIDEHUM_MAND = 0, AIDEHUM_GRE = 0, AIDEHUM_NOT = 0, AIDEHUM_HEURE = 0)]

  return(dt_)

}



predict_multinom <- function(modele_, donnees_a_predire_){
  #' predict_multinom
  #'
  #' Imputation d'une modalité de type de service employé
  #' Avec un modèle multinomial univarié selon le tarif moyen d'une heure d'aide humaine
  #' Pour les individus dont le tarif moyen d'une heure d'aide humaine est manquant,
  #' on applique un modèle multinomial univarié selon le montant en aide humaine dans le plan
  #'
  #' @param modele_ model, sortie d'un fit de modèle multinomial
  #' @param donnees_a_predire_ data.table, données à prédire
  #' @export
  #'
  #' @importFrom data.table :=
  #'

  probs <- predict(modele_,donnees_a_predire_,"probs")
  cum.probs <- t(apply(probs,1,cumsum))

  # Tirage de valeurs aléatoires
  vals <- runif(nrow(donnees_a_predire_))

  # Jointure des proba et des valeurs aléatoires
  tmp <- cbind(cum.probs,vals)

  # Pour chaque ligne, sélection du bon index
  k <- ncol(probs)
  ids <- colnames(probs)[1 + apply(tmp,1,function(x) length(which(x[1:k] < x[k+1])))]

  return(ids)

}

imputation_type_service <- function(dt_, gir_) {

  #' imputation_type_service
  #'
  #' Imputation d'une modalité de type de service employé
  #' Avec un modèle multinomial univarié selon le tarif moyen d'une heure d'aide humaine
  #' Pour les individus dont le tarif moyen d'une heure d'aide humaine est manquant,
  #' on applique un modèle multinomial univarié selon le montant en aide humaine dans le plan
  #'
  #' @param dt_ data.table, données à transformer
  #' @param gir_ numeric, GIR considéré
  #' @export
  #'
  #' @importFrom data.table :=
  #' @importFrom nnet multinom
  #'

  # étape 1 : en fonction du tarif horaire
  m1 <- multinom(type_inter ~ TARIF_AIDEHUM,
                 data=dt_[gir_imput == gir_ & AIDEHUM==1 & type_inter != "non connu"],
                 trace=FALSE)

  newdata1 <- dt_[gir_imput == gir_ & AIDEHUM==1 & type_inter == "non connu" & !is.na(TARIF_AIDEHUM)]
  dt_[gir_imput == gir_ & AIDEHUM==1 & type_inter == "non connu" & !is.na(TARIF_AIDEHUM),
      type_inter := predict_multinom(m1, newdata1)]

  # étape 2 : pour ceux qui sont toujours manquants, en fonction du montant d'aide humaine
  m2 <- multinom(type_inter ~ AIDEHUM_NOT,
                 data=dt_[gir_imput == gir_ & AIDEHUM==1 & type_inter != "non connu"],
                 trace=FALSE)

  newdata2 <- dt_[gir_imput == gir_ & AIDEHUM==1 & type_inter == "non connu"]
  dt_[gir_imput == gir_ & AIDEHUM==1 & type_inter == "non connu",
      type_inter := predict_multinom(m2, newdata2)]

  return(dt_)
}

remise_coherence_indicatrices_type_service <- function(dt_){

  #' remise_coherence_indicatrices_type_service
  #'
  #' Affectation des indicatrices de type de service d'aide humaine employé pour
  #' mise en cohérence avec la variable "type_inter" de type d'emploi
  #'
  #' @param dt_ data.table, données à transformer
  #' @export
  #'
  #' @importFrom data.table :=
  #'

  dt_[type_inter %in% c("presta", "presta et mand", "presta et gre", "les 3"), AIDEHUM_PRESTA := 1]
  dt_[type_inter %in% c("mand", "presta et mand", "mand et gre", "les 3"), AIDEHUM_MAND := 1]
  dt_[type_inter %in% c("gre a gre", "mand et gre", "presta et gre", "les 3"), AIDEHUM_GRE := 1]

  return(dt_)
}


construction_indic_mode_emploi <- function(dt_) {

  #' construction_indic_mode_emploi
  #'
  #' construction de l'indicatrice type de service d'aide humaine
  #' passage des "9" dans les colonnes indicatrices en NA
  #' @param dt_ data.table, données à transformer
  #'
  #' @export
  #'
  #' @importFrom dplyr case_when
  #' @importFrom data.table data.table :=
  #'

  dt_[, type_inter := case_when(
    AIDEHUM_PRESTA==1 & AIDEHUM_MAND==0 & AIDEHUM_GRE==0 ~ "presta",
    AIDEHUM_PRESTA==1 & AIDEHUM_MAND==9 & AIDEHUM_GRE==0 ~ "presta",
    AIDEHUM_PRESTA==1 & AIDEHUM_MAND==0 & AIDEHUM_GRE==9 ~ "presta",
    AIDEHUM_PRESTA==1 & AIDEHUM_MAND==9 & AIDEHUM_GRE==9 ~ "presta",
    AIDEHUM_PRESTA==0 & AIDEHUM_MAND==1 & AIDEHUM_GRE==0 ~ "mand",
    AIDEHUM_PRESTA==9 & AIDEHUM_MAND==1 & AIDEHUM_GRE==0 ~ "mand",
    AIDEHUM_PRESTA==0 & AIDEHUM_MAND==1 & AIDEHUM_GRE==9 ~ "mand",
    AIDEHUM_PRESTA==0 & AIDEHUM_MAND==9 & AIDEHUM_GRE==9 ~ "mand",
    AIDEHUM_PRESTA==0 & AIDEHUM_MAND==0 & AIDEHUM_GRE==1 ~ "gre a gre",
    AIDEHUM_PRESTA==0 & AIDEHUM_MAND==1 & AIDEHUM_GRE==1 ~ "mand et gre",
    AIDEHUM_PRESTA==9 & AIDEHUM_MAND==1 & AIDEHUM_GRE==1 ~ "mand et gre",
    AIDEHUM_PRESTA==1 & AIDEHUM_MAND==0 & AIDEHUM_GRE==1 ~ "presta et gre",
    AIDEHUM_PRESTA==1 & AIDEHUM_MAND==9 & AIDEHUM_GRE==1 ~ "presta et gre",
    AIDEHUM_PRESTA==1 & AIDEHUM_MAND==1 & AIDEHUM_GRE==0 ~ "presta et mand",
    AIDEHUM_PRESTA==1 & AIDEHUM_MAND==1 & AIDEHUM_GRE==9 ~ "presta et mand",
    AIDEHUM_PRESTA==1 & AIDEHUM_MAND==1 & AIDEHUM_GRE==1 ~ "les 3",
    AIDEHUM_PRESTA==0 & AIDEHUM_MAND==0 & AIDEHUM_GRE==0 ~ "aucun",
    TRUE ~ "non connu"
  )]

  dt_[, TARIF_AIDEHUM := AIDEHUM_NOT / AIDEHUM_HEURE]
  dt_[AIDEHUM==0, type_inter := "aucun"]

  # les types d'emploi non connus sont imputés avec un modèle multinomial
  imputation_type_service(dt_, 1)
  imputation_type_service(dt_, 2)
  imputation_type_service(dt_, 3)
  imputation_type_service(dt_, 4)

  remise_coherence_indicatrices_type_service(dt_)

  cols_rep <- c("AUTREAIDE", "AIDEPONC", "AIDEHEB","AIDEACC","AIDETRANS", "AIDEHUM", "AIDEHUM_PRESTA",
                "AIDEHUM_MAND", "AIDEHUM_GRE")

  dt_[, (cols_rep) := replace(.SD, .SD == 9, NA), .SDcols = cols_rep]

  return(dt_)

}

corr_presta_wins <- function(dt_, niveau_ = 0.075) {

  #' corr_presta_wins
  #'
  #' Correction des tarifs en mode d'emploi prestataire
  #' Winsorisation : on affecte la valeur du quantile de niveau niveau_ aux niveau_% des valeurs les plus basses,
  #' et la valeur du quantile de niveau 1-niveau_ au niveau_% des valeurs les plus hautes
  #'
  #' @param dt_ data.table, données à transformer
  #' @param niveau_ numeric, entre 0 et 1, niveau du quantile à redresser
  #'
  #' @importFrom stats quantile
  #' @importFrom data.table :=

  # On enlève les 2*niveau_% des valeurs (des plus hautes et plus basses valeurs) : PRESTA
  valmin <- quantile(dt_$TARIF_PRESTA, niveau_, na.rm = TRUE)
  valmax <- quantile(dt_$TARIF_PRESTA, 1-niveau_, na.rm = TRUE)

  dt_[TARIF_PRESTA < valmin, TARIF_PRESTA := valmin]
  dt_[TARIF_PRESTA > valmax, TARIF_PRESTA := valmax]

  return(dt_)

}

corr_mand_wins <- function(dt_, niveau_ = 0.075) {

  #' corr_mand_wins
  #'
  #' Correction des tarifs en mode d'emploi mandataire
  #' Winsorisation : on affecte la valeur du quantile de niveau niveau_ aux niveau_% des valeurs les plus basses,
  #' et la valeur du quantile de niveau 1-niveau_ au niveau_% des valeurs les plus hautes
  #'
  #' @param dt_ data.table, données à transformer
  #' @param niveau_ numeric, entre 0 et 1, niveau du quantile à redresser
  #'
  #' @importFrom stats quantile
  #' @importFrom data.table :=

  # On enlève les 15% des valeurs (des plus hautes et plus basses valeurs) : MAND
  valmin <- quantile(dt_$TARIF_MAND, niveau_, na.rm = TRUE)
  valmax <- quantile(dt_$TARIF_MAND, 1-niveau_, na.rm = TRUE)

  dt_[TARIF_MAND < valmin, TARIF_MAND := valmin]
  dt_[TARIF_MAND > valmax, TARIF_MAND := valmax]

  return(dt_)

}


corr_gre_wins <- function(dt_, niveau_ = 0.075) {

  #' corr_gre_wins
  #'
  #' Correction des tarifs en mode d'emploi gré à gré
  #' Winsorisation : on affecte la valeur du quantile de niveau niveau_ aux niveau_% des valeurs les plus basses,
  #' et la valeur du quantile de niveau 1-niveau_ au niveau_% des valeurs les plus hautes
  #'
  #' @param dt_ data.table, données à transformer
  #' @param niveau_ numeric, entre 0 et 1, niveau du quantile à redresser
  #'
  #' @importFrom stats quantile
  #' @importFrom data.table :=

  # On enlève les 15% des valeurs (des plus hautes et plus basses valeurs) : GRE
  valmin <- quantile(dt_$TARIF_GRE, niveau_, na.rm = TRUE)
  valmax <- quantile(dt_$TARIF_GRE, 1-niveau_, na.rm = TRUE)

  dt_[TARIF_GRE < valmin, TARIF_GRE := valmin]
  dt_[TARIF_GRE > valmax, TARIF_GRE := valmax]

  return(dt_)

}



reconstruction_heures <- function(dt_) {

  #' reconstruction_heures
  #'
  #' Estimation du nombre d'heure d'aide humaine consommée en chaque mode d'emploi (prestataire, mandataire ou gré à gré)
  #' Si le bénéficiaire n'a qu'un type d'emploi, le nombre d'heure est directement le montant total divisé par le tarif du mode d'emploi
  #' (on n'utilise pas directement la variable du nombre d'heures d'aide humaine, car on a opéré un redressement sur les tarifs horaires)
  #'
  #' Si le tarif horaire est manquant, on utilise le inférence a partir du tarif horaire médian du type d'emploi
  #' Si le bénéficiaire a plusieurs types d'emploi, on ne connait pas le tarif horaire de chacun des modes
  #' On utilise la médiane du tarif horaire des bénéficiaires d'un seul type d'emploi
  #' On fait l'hypothèse que les bénéficiaires de plusieurs types d'emploi consomment le même nombre d'heure dans chacun des types d'emploi
  #'
  #' Création colonne avec les tarifs horaires corrigés.
  #' Calcul du nombre total d'heures.
  #'
  #' @param dt_ data.table, données à analyser
  #'
  #' @export
  #'
  #' @importFrom stats weighted.mean
  #' @importFrom data.table :=
  #'

  mean_tarifpresta <- weighted.mean(dt_$TARIF_PRESTA, w = dt_$poids_ah, na.rm = TRUE)
  mean_tarifmand <- weighted.mean(dt_$TARIF_MAND, w = dt_$poids_ah,na.rm = TRUE)
  mean_tarifgre <- weighted.mean(dt_$TARIF_GRE, w = dt_$poids_ah, na.rm = TRUE)

  # un seul type d'emploi
  dt_[type_inter == "presta", HEURES_PREST_COR := AIDEHUM_NOT / TARIF_PRESTA]
  dt_[type_inter == "mand", HEURES_MAND_COR := AIDEHUM_NOT / TARIF_MAND]
  dt_[type_inter == "gre a gre", HEURES_GRE_COR := AIDEHUM_NOT / TARIF_GRE]


  dt_[type_inter == "presta" & is.na(AIDEHUM_HEURE), HEURES_PREST_COR := AIDEHUM_NOT / mean_tarifpresta]
  dt_[type_inter == "mand" & is.na(AIDEHUM_HEURE), HEURES_MAND_COR := AIDEHUM_NOT / mean_tarifmand]
  dt_[type_inter == "gre a gre" & is.na(AIDEHUM_HEURE), HEURES_GRE_COR := AIDEHUM_NOT / mean_tarifgre]

  # plusieurs types d'emploi
  dt_[type_inter == "presta et gre", `:=`(HEURES_PREST_COR = AIDEHUM_NOT / (mean_tarifpresta + mean_tarifgre),
                                          HEURES_GRE_COR = AIDEHUM_NOT / (mean_tarifpresta + mean_tarifgre))]

  dt_[type_inter == "presta et mand", `:=`(HEURES_PREST_COR = AIDEHUM_NOT / (mean_tarifpresta + mean_tarifmand),
                                           HEURES_MAND_COR = AIDEHUM_NOT / (mean_tarifpresta + mean_tarifmand))]

  dt_[type_inter == "mand et gre", `:=`(HEURES_GRE_COR = AIDEHUM_NOT / (mean_tarifgre + mean_tarifmand),
                                        HEURES_MAND_COR = AIDEHUM_NOT / (mean_tarifgre + mean_tarifmand))]

  dt_[type_inter == "les 3", `:=`(HEURES_GRE_COR = AIDEHUM_NOT / (mean_tarifgre + mean_tarifmand + mean_tarifpresta),
                                  HEURES_MAND_COR = AIDEHUM_NOT / (mean_tarifgre + mean_tarifmand + mean_tarifpresta),
                                  HEURES_PREST_COR = AIDEHUM_NOT / (mean_tarifgre + mean_tarifmand + mean_tarifpresta))]

  # création colonne avec les tarifs horaires corrigés
  dt_[, `:=`(TARIF_PRESTA_COR = TARIF_PRESTA,
             TARIF_MAND_COR = TARIF_MAND,
             TARIF_GRE_COR = TARIF_GRE)]

  dt_[is.na(TARIF_PRESTA_COR) & AIDEHUM_PRESTA ==1, TARIF_PRESTA_COR := mean_tarifpresta]
  dt_[is.na(TARIF_MAND_COR)& AIDEHUM_MAND ==1, TARIF_MAND_COR := mean_tarifmand]
  dt_[is.na(TARIF_GRE_COR) & AIDEHUM_GRE ==1, TARIF_GRE_COR := mean_tarifgre]

  # calcul du nombre total d'heures
  dt_[type_inter == "presta", AIDEHUM_HEURE_COR := HEURES_PREST_COR]
  dt_[type_inter == "mand", AIDEHUM_HEURE_COR := HEURES_MAND_COR]
  dt_[type_inter == "gre a gre", AIDEHUM_HEURE_COR := HEURES_GRE_COR]

  dt_[type_inter == "presta et gre", AIDEHUM_HEURE_COR := HEURES_PREST_COR + HEURES_GRE_COR]
  dt_[type_inter == "presta et mand", AIDEHUM_HEURE_COR := HEURES_PREST_COR + HEURES_MAND_COR]
  dt_[type_inter == "mand et gre", AIDEHUM_HEURE_COR := HEURES_MAND_COR + HEURES_GRE_COR]
  dt_[type_inter == "les 3", AIDEHUM_HEURE_COR := HEURES_PREST_COR + HEURES_MAND_COR + HEURES_GRE_COR]

  return(dt_)
}

estimation_tarif_horaire <- function(dt_){

  #' estimation_tarif_horaire
  #'
  #' Fonction qui calcule le tarif horaire de chaque mode d'emploi pour chaque bénéficiaire du mode d'emploi
  #' Et affectation du nombre d'heures associé pour comparaison future avec le nb d'heures corrigé
  #'
  #' @param dt_ data.table, données à analyser
  #'
  #' @export
  #'
  #' @importFrom data.table :=
  #'

  dt_[type_inter=="presta",`:=`(TARIF_PRESTA = AIDEHUM_NOT / AIDEHUM_HEURE,
                                  HEURES_PRESTA = AIDEHUM_HEURE)]
  dt_[type_inter=="mand", `:=`(TARIF_MAND = AIDEHUM_NOT / AIDEHUM_HEURE,
                                 HEURES_MAND = AIDEHUM_HEURE)]
  dt_[type_inter=="gre a gre", `:=`(TARIF_GRE = AIDEHUM_NOT / AIDEHUM_HEURE,
                                      HEURES_GRE = AIDEHUM_HEURE)]

  return(dt_)
}


ajout_traitement_nb_heures <- function(dt_, niveau_=0.075) {

  #' ajout_traitement_nb_heures
  #'
  #' Fonction qui gère le nettoyage du nombre d'heures d'aide humaine des différents types d'emploi
  #'
  #' @param dt_ data.table, données à analyser
  #'
  #' @export
  #'
  #' @importFrom data.table :=
  #'

  dtah <- dt_[AIDEHUM == 1] # bénéficiaires d'aide humaine
  dt_hors_ah <- dt_[AIDEHUM == 0] # non bénéficiaires d'aide humaine

  # Indicatrice aide humaine manquante mais tous les autres type d'aide a 0 (non manquants), on considère qu'ils sont bénéf d'aide humaine
  # Logique de priorisation de l'aide humaine
  dtmanq <- dt_hors_ah[AIDEHUM==0 & AIDEPONC == 0 & AUTREAIDE == 0 & AIDEACC == 0]
  dt_hors_ah <- dt_hors_ah[!(AIDEHUM==0 & AIDEPONC == 0 & AUTREAIDE == 0 & AIDEACC == 0)]
  dt_hors_ah[, poids_ah := poids_p]

  # alors on répartit leurs poids sur les autres bénéficiaires d'aide humaine
  poids_tot <- sum(dtmanq$poids_p)
  # correction des poids avec les bénéficiaires d'aide humaine manquants
  dtah[, poids_ah := poids_p + poids_tot/nrow(dtah)]

  dtah2 <- estimation_tarif_horaire(dtah)

  dtah3 <- corr_presta_wins(dtah2, niveau_)
  dtah4 <- corr_mand_wins(dtah3, niveau_)
  dtah5 <- corr_gre_wins(dtah4, niveau_)

  dtah6 <- reconstruction_heures(dtah5)

  res <- data.table(bind_rows(dtah6, dt_hors_ah))

  return(res)
}



deplafonnement_plan_2017 <- function(dt_, niveau_pred_=0.96){

  #' deplafonnement_plan_2017
  #'
  #' TOBIT pour l'effet de l'augmentation des plafonds, prédiction avec les anciens plans et les anciens plafonds
  #'
  #' @param dt_ data.table
  #' @param niveau_pred_ numeric, entre 0 et 1, valeur du ratio plan/plafond à partir duquel on simule le "vrai" besoin
  #'
  #' @export
  #'
  #' @importFrom stats predict rnorm
  #' @importFrom ATNMXOutils fit_tobit
  #' @importFrom data.table :=

  dt_[, `:=`(ratio_reel = plan_noti_2017 / PLAFOND_2017,
             ratio_plan_plafond = pmin(plan_noti_2017 / PLAFOND_2017, 0.96),
             age_plus60 = AGE_C - 60,
             duree_apad_m = as.Date("2017-12-31") - as.Date(DATE_APAD),
             ressource_apa = ifelse(is.na(ressource_apa), weighted.mean(ressource_apa, w = poids_ah, na.rm = TRUE), ressource_apa),
             indicatrice_couple = ifelse(is.na(couple), 3, couple),
             IDENT_SEN = ID)]

  tob1 <- fit_tobit(dt_, 1)
  tob2 <- fit_tobit(dt_, 2)
  tob3 <- fit_tobit(dt_, 3)
  tob4 <- fit_tobit(dt_, 4)

  dt_ <- deplafonnement(dt_, 1, tob1, niveau_pred_)
  dt_ <- deplafonnement(dt_, 2, tob2, niveau_pred_)
  dt_ <- deplafonnement(dt_, 3, tob3, niveau_pred_)
  dt_ <- deplafonnement(dt_, 4, tob4, niveau_pred_)

  dt_[, indicatrice_couple := NULL]
  dt_[, age_plus60 := NULL]
  dt_[, duree_apad_m := NULL]
  dt_[, IDENT_SEN := NULL]

  return(dt_)
}

deplafonnement <- function(dt_, gir_, tobit_model_, niveau_pred_){

  #' deplafonnement
  #'
  #' Pour les individus qui ont un ratio plan/plafond supérieur à niveau_pred_,
  #' on simule la valeur du ratio besoin/plafond
  #'
  #' @param dt_ data.table, données à modifier
  #' @param gir_ integer, gir des bénéficiaires concernés
  #' @param tobit_model_ model, modèle tobit entrainé
  #' @param niveau_pred_ numeric, entre 0 et 1, valeur du ratio plan/plafond à partir duquel on simule le "vrai" besoin
  #'
  #' @export
  #' @importFrom data.table :=

  # Hypothèse : la répartition du montant des plans est symétrique
  # Premier signe : mean et médiane extremement proches
  # Le tobit doit être entrainé avec les modalités les plus fréquentes des variables de contrôle
  # Moyenne du tobit n'est pas biaisée : intérêt du tobit
  unbiased_mean <- mean(tobit_model_$yhat$yhat)

  # standard deviation sur la moitié inférieure de la distribution : fonctionne sous l'hypothèse d'une distribution sym
  est_sd <- sqrt(sum((dt_[gir_imput == gir_ & ratio_plan_plafond <= unbiased_mean]$ratio_plan_plafond - unbiased_mean)^2)/
                   (length(dt_[gir_imput == gir_ & ratio_plan_plafond <= unbiased_mean]$ratio_plan_plafond) - 1))

  # Dans un modèle tobit, le modèle sous-jacent sans censure est une normale centrée réduite
  # on simule selon cette "vraie" loi
  pred1 <- rnorm(n = 3*nrow(dt_[gir_imput == gir_]), mean = unbiased_mean, sd = est_sd)

  dt_[gir_imput == gir_, pred_ratio := ratio_reel]
  dt_[gir_imput == gir_ & ratio_reel >= niveau_pred_,
        pred_ratio := pred1[pred1>=niveau_pred_][1:nrow(dt_[gir_imput == gir_ & ratio_reel >= niveau_pred_])]]

  dt_[gir_imput == gir_, pred_besoin_aide := pred_ratio*PLAFOND_2017]

  return(dt_)

}
