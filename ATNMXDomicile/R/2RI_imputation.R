# Copyright (C) 2021-2022. Logiciel élaboré par l'État, via la Drees.

# Pole Simulation du bureau Handicap et Dépendance, Drees.

# Ce programme informatique a été développé par la Drees. Il permet d'exécuter le modèle de microsimulation Autonomix version 1.0.1 de juillet 2022.

# Ce programme a été exécuté le 19/07/2022 avec la version 4.0.3 de R et les packages mobilisés sont listés dans le fichier DESCRIPTION du projet »

# La documentation du modèle peut être consultés sur [gitlab](https://drees_code.gitlab.io/public/modeles/autonomix/DocumentationATNMX/)

# Ce programme utilise les données de la base CARE Ménage 2015, ou des RI 2017 ou des RI 2017 anonymisées dans leur version du 23 décembre 2021

# Bien qu'il n'existe aucune obligation légale à ce sujet, les utilisateurs de ce programme sont invités à signaler à la DREES leurs travaux issus de la réutilisation de ce code, ainsi que les éventuels problèmes ou anomalies qu'ils y rencontreraient, en écrivant à DREES-CODE@sante.gouv.fr

# Ce logiciel est régi par la licence EUPL V1.2 soumise au droit français et européen, respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence EUPL V1.2, telle que diffusée sur https://eupl.eu/1.2/fr/ et reproduite dans le fichier LICENCE diffusé conjointement au présent programme.
# En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.
# À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.


lancement_imputations_ri <- function(chemin_constitution_, chemin_imputation_) {
  #' lancement_imputations_ri
  #'
  #' Procède aux imputation nécessaires pour les remontées individuelles
  #'
  #' @param chemin_constitution_ character, chemin vers les bases construites à partir des données brutes
  #' @param chemin_imputation_ character, chemin où écrire les bases issues de l'imputation
  #'
  #' @return data.frame contenant les données ri imputées
  #'
  #' @importFrom descr freq
  #' @importFrom dplyr filter mutate left_join rename select n bind_rows
  #' @importFrom lubridate time_length interval ymd
  #' @importFrom AER tobit
  #' @importFrom stats glm binomial gaussian predict quantile rnorm
  #' @importFrom magrittr %>%
  #' @importFrom data.table := fread fwrite setnames
  #' @importFrom survival survreg Surv
  #'
  #' @export
  #'
  #'

  dfri <- fread(paste(chemin_constitution_, "data_ri2017_constitution_base.csv", sep="/"))

  setnames(dfri,
           old=c("SEXE", "GIRFIN_APA", "AGER"),
           new=c("SEXE_C", "gir_imput", "AGE_C"))

  dfri[, IDENT_SEN := ID]

  dfri <- dfri[!(DEP %in% c("002","082"))] # plus de 20% de manquants sur les indic d'aide humaine

  # Suppression des individus à situation de couple inconnue et répartition des poids ####
  # avant de supprimer les couples=9, on repondère les obs.
  poids_couple_01 <- nrow(dfri[!(couple%in%9)])
  poids_couple_9 <- nrow(dfri[(couple%in%9)])

  poids_c <- poids_couple_9/poids_couple_01

  # affectation des poids calculés à la base des droits ouverts
  dfri[couple%in%9, poids_p_c := 0]
  dfri[!(dfri$couple%in%9), poids_p_c := poids_p + poids_c]

  dfri <- dfri[couple!=9]
  #594907 obs
  sum(dfri$poids_p_c) # 766 905
  sum(dfri$poids_p)  # 738 576

  #766 905 individus pondérés
  dfri[, couple_rfs:=  case_when(
    RESSOURC_APA_IND==0 & couple==0 & TYPRESS_APA==2 ~ 1,
    RESSOURC_APA_IND==0 & couple==0 & TYPRESS_APA==3 ~ 1,
    TRUE ~ as.numeric(couple))]

  dfri[, couple_rfs := as.factor(couple_rfs)]



  # Imputation des ressources ------
  dfri[, age_plus60 := AGE_C - 60]
  dfri[, age_plus60_carre := age_plus60^2]

  dfri[, RESSOURAPA_non_impute:=RESSOURC_APA]
  dfri[,taux_particip:=case_when(!is.na(APA) & !is.na(NOTPB)  ~ NOTPB/APA ,
                                 TRUE  ~ NA_real_
  )]

  # summary(dfri$taux_particip,useNA="always")
  # test <- dfri %>% filter(RESSOURC_APA>=30000) # 21 obs
  # freq(test$DEP,plot=F,useNA="always")# dont 8 dans le 75, cela parait juste


  # glm pour imputer les ressources
  # entrainement seulement pour le valeurs>0 et < 30000

  # test <- dfri %>% filter(RESSOURAPA_non_impute==0)
  # #5401 obs

  model_glm <- glm(RESSOURAPA_non_impute ~ couple_rfs + SEXE_C + age_plus60 + age_plus60_carre+DEP,
                   data = dfri[RESSOURAPA_non_impute>0 & RESSOURAPA_non_impute<30000],
                   na.action = "na.exclude",
                   family = gaussian(link = "log"))

  ressource_apa_imput_glm <- predict(model_glm, newdata = dfri, type="response")
  dfri[, ress_glm := ressource_apa_imput_glm]
  dfri[is.na(RESSOURC_APA), RESSOURC_APA := ress_glm]
  dfri[, ress_glm := NULL]

  # summary(dfri$RESSOURC_APA,useNA="always")
  # summary(dfri$RESSOURAPA_non_impute,useNA="always")
  # quantile(as.numeric(dfri$RESSOURAPA_non_impute),na.rm=TRUE,seq(0.1,1,by=0.1))
  # quantile(as.numeric(dfri$RESSOURC_APA),na.rm=TRUE,seq(0.1,1,by=0.1))


  # imputation du plan-------

  #correction plan, AIDEHUM_NOT et part AH
  dfri[, plan_2017 := APA]
  dfri[, APA := NULL]
  # summary(dfri$plan_2017,useNA="always") #31 417 NA
  # summary(dfri$AIDEHUM_NOT,useNA="always")

  # borne du montant en aide humaine par le montant du plan total
  dfri[plan_2017<AIDEHUM_NOT, AIDEHUM_NOT:= plan_2017]
  dfri[AIDEHUM==0, AIDEHUM_NOT:= 0]

  dfri[, proportion_ah_annee_base:=AIDEHUM_NOT/plan_2017]
  #4817 qui sont manquant qu'on peut mettre à 0
  dfri[AIDEHUM==0, proportion_ah_annee_base :=  0]

  #s'il n'y a que de l'aide humaine, on corrige les variables
  dfri[AIDEHUM==1 & AUTREAIDE==0 & AUTREAIDE_I==0 & AIDEPONC_I==0 & AIDEHEB==0 & AIDEACC==0 & AIDETRANS==0,
       proportion_ah_annee_base := 1]

  # on peut corriger le plan par AIDEHUM_NOT qd plan_2017 manquant et qu'il n'y a que de l'aide hu
  #6148 obs
  # on peut corriger AIDEHUM_NOT par le plan qd AIDEHUM_NOT manquante il n'y a que de l'aide hu
  #17 569 NA

  dfri[AIDEHUM==1 & AUTREAIDE==0 & AIDEPONC==0 & AIDEHEB==0 & AIDEACC==0 & AIDETRANS==0
       & !is.na(AIDEHUM_NOT) & is.na(plan_2017), plan_2017 := AIDEHUM_NOT]

  dfri[AIDEHUM==1 & AUTREAIDE==0 & AIDEPONC==0 & AIDEHEB==0 & AIDEACC==0 & AIDETRANS==0
       & is.na(AIDEHUM_NOT) & !is.na(plan_2017),AIDEHUM_NOT := plan_2017]

  dfri[, plan_2017_non_imput:=plan_2017]

  # Calcul de la durée de l'APA en mois jusque décembre 2017
  dfri[, DATE_APAD := ymd(DATE_APAD)]

  date_3112 <- ymd("2017-12-31")
  dfri[DATE_APAD==ymd("9998-12-30") | DATE_APAD>date_3112, DATE_APAD := NA]

  dfri[, duree_apad_m := time_length(interval(start = ymd(DATE_APAD), end = date_3112), unit = "months")]

  dfri[, duree_apad_t := case_when(
    duree_apad_m < 12 ~ "Moins d'1 an",
    12 <= duree_apad_m & duree_apad_m<24 ~ "[1-2 ans)",
    24<= duree_apad_m & duree_apad_m<36 ~ "[2-3 ans)",
    36<= duree_apad_m & duree_apad_m<48 ~  "[3-4 ans)",
    48 <= duree_apad_m & duree_apad_m<60 ~ "[4-5 ans)",
    60 <= duree_apad_m & duree_apad_m<72 ~ "[5-6 ans)",
    72 <= duree_apad_m & duree_apad_m<96 ~ "[6-8 ans)",
    96 <=duree_apad_m ~  "8 ans et +",
    is.na(duree_apad_m)  ~ "Manquant")]

  dfri[, duree_apa_m := duree_apad_m]
  dfri[, duree_apad_m := NULL]

  # freq(dfri$duree_apad_t,useNA="always",plot=F)

  dfri[, gir_imput := as.integer(gir_imput)]

  #modele Tobit pour le plan
  dfri[, PLAFOND_2017:=case_when(
    gir_imput==1 ~ 1714.79,
    gir_imput==2 ~ 1376.91,
    gir_imput==3 ~ 994.87,
    gir_imput==4 ~ 663.61
  )]

  dfri[, ressource_apa := RESSOURC_APA]
  dfri[, indicatrice_couple := couple]

  dfri[, ratio_plan_plafond := pmin(plan_2017 / PLAFOND_2017, 0.96)]

  # Je crée une fonction qui filtre le dataframe en ne prenant que le gir num_GIR comme parametre
  set.seed(123)
  fit_tobit_plan <- function(dfri1, num_GIR,left_cens = 0,  right_cens = 0.96){

    df_temp <- dfri1[gir_imput == num_GIR]

    model <- tobit(ratio_plan_plafond ~
                     age_plus60 + SEXE_C + duree_apad_t + couple_rfs + RESSOURC_APA ,
                   dist ="gaussian",
                   left=left_cens,
                   right = right_cens,
                   data = df_temp,
                   na.action = "na.exclude")

    yhat <- predict(model, newdata = df_temp )
    yhat <- data.frame("ID" = df_temp$ID,
                       "y" = as.numeric(yhat))

    return(list(model = model, yhat = yhat))
  }

  # On met les variables continues pour augmenter le nbre de degrés de liberté
  # on applique la fonction fit_tobit au GIR1
  tob1 <- fit_tobit_plan(dfri, 1)
  # summary(tob1[["model"]])
  # on applique la fonction fit_tobit au GIR2
  tob2 <- fit_tobit_plan(dfri, 2)
  # summary(tob2[["model"]])
  # on applique la fonction fit_tobit au GIR3
  tob3 <- fit_tobit_plan(dfri, 3)
  # summary(tob3[["model"]])
  # on applique la fonction fit_tobit au GIR4
  tob4 <- fit_tobit_plan(dfri, 4)
  # summary(tob4[["model"]])

  # On groupe les prédictions issues de chaque modèle en une table avec l'identifiant et la prédiction
  fitted <- rbind(tob1[["yhat"]], tob2[["yhat"]], tob3[["yhat"]], tob4[["yhat"]])

  # On ajoute le besoin d'aide imputé à partir de la prédiction Tobit
  # pour cela on left_join la table apa_domicile_3 avec le fitted par la clef ID
  dfri_1 <- data.table(merge(dfri, fitted, by="ID"))
  print(paste0("TEST 1 : ", (nrow(dfri) == nrow(dfri_1))))

  setnames(dfri_1, "y", "ratio_plan_plafond_imput")

  scale_t <- c(
    tob1[["model"]]$scale, tob2[["model"]]$scale,
    tob3[["model"]]$scale, tob4[["model"]]$scale
  )

  sd_ratio <- 1
  set.seed(123)
  alea1 <- rnorm(n = nrow(dfri_1), sd = sd_ratio)
  alea2 <- rnorm(n = nrow(dfri_1), sd = sd_ratio)
  alea3 <- rnorm(n = nrow(dfri_1), sd = sd_ratio)
  alea4 <- rnorm(n = nrow(dfri_1), sd = sd_ratio)
  alea5 <- rnorm(n = nrow(dfri_1), sd = sd_ratio)

  # ajout alea dans part plan plafond prédit par Tobit
  dfri_1[,
      part_plan_alea :=
        ajout_alea_plan(
          scale_t,
          ratio_plan_plafond_imput,
          1 * (gir_imput == 1) + 2 * (gir_imput == 2) +
            3 * (gir_imput == 3) + 4 * (gir_imput == 4),
          alea1, alea2, alea3, alea4, alea5
        )]

  # convention : le plan doit pouvoir financer au moins une heure d'aide en mode gré à gré
  # donc pas de plan en dessous de 14 €

  dfri_1[, ratio_plan_plafond_imput := pmax(part_plan_alea, 14/PLAFOND_2017)]

  dfri_1[, plan_Deplaf_2017 :=
             ratio_plan_plafond_imput * PLAFOND_2017]

  dfri_1[, plan_Replaf_2017  := case_when(
    plan_Deplaf_2017 >
      PLAFOND_2017 ~ PLAFOND_2017,
    TRUE ~ plan_Deplaf_2017
  )]

  #plan_Replaf_2017 : si on impute pour tous le monde
  # plan_2017 : si on impute que les manquants
  dfri_1[is.na(plan_2017_non_imput), plan_2017 := plan_Replaf_2017]

  dfri_1[, plan_2017 := ifelse(plan_2017<AIDEHUM_NOT & AIDEHUM_NOT<PLAFOND_2017 & !is.na(AIDEHUM_NOT),
                               AIDEHUM_NOT,
                               plan_2017)]

  dfri_1[, aide_conc := paste0(AIDEHUM,"_" ,AUTREAIDE,"_" , AIDEPONC,"_",AIDEHEB,"_",AIDEACC,"_" ,AIDETRANS)]

  dfri_1[, plan_noti_2017 := case_when(
    !is.na(plan_2017_non_imput) | is.na(AIDEHUM_NOT) ~ plan_2017,

    is.na(plan_2017_non_imput) & !is.na(AIDEHUM_NOT) & PLAFOND_2017<AIDEHUM_NOT ~ AIDEHUM_NOT,

    is.na(plan_2017_non_imput) & !is.na(AIDEHUM_NOT) & PLAFOND_2017>=AIDEHUM_NOT & plan_2017 >=AIDEHUM_NOT ~ plan_2017,

    is.na(plan_2017_non_imput) & !is.na(AIDEHUM_NOT) & PLAFOND_2017>=AIDEHUM_NOT
    & plan_2017<AIDEHUM_NOT & aide_conc %in% c("1_0_0_9_9_9","1_0_9_0_0_9","1_0_9_9_9_9","1_9_9_0_0_0")
    ~ AIDEHUM_NOT,

    is.na(plan_2017_non_imput) & !is.na(AIDEHUM_NOT) & PLAFOND_2017>=AIDEHUM_NOT
    & plan_2017<AIDEHUM_NOT &
      !(aide_conc %in% c("1_0_0_9_9_9","1_0_9_0_0_9","1_0_9_9_9_9","1_9_9_0_0_0"))
    ~ runif(1,AIDEHUM_NOT,PLAFOND_2017),
    TRUE ~ plan_2017
  )]

  dfri_1[, aide_conc := NULL]

  # summary(dfri_1$plan_noti_2017)
  # test <- dfri_1 %>% filter(plan_noti_2017!=plan_2017)#3749
  #
  # test <- dfri_1 %>% filter(is.na(plan_noti_2017))#0
  # test <- dfri_1 %>% filter(plan_noti_2017<AIDEHUM_NOT)#0
  # test <- dfri_1 %>% filter(plan_2017<AIDEHUM_NOT)#3749
  # test <- dfri_1 %>% filter(plan_2017>PLAFOND_2017)#3749
  # test <- dfri_1 %>% filter(plan_noti_2017>PLAFOND_2017)#14433

  dfri_1[, plan_2017 := NULL]

  #recalcul de proportion_ah_annee_base
  dfri_1[, proportion_ah_annee_base := AIDEHUM_NOT/plan_noti_2017]

  #pour l'instant on complète les manquantes à 1
  dfri_1[, proportion_ah_annee_base:=case_when(
    is.na(proportion_ah_annee_base)  ~ 1,
    TRUE~ proportion_ah_annee_base
  )]


  dfri_1[, retraite_foyer:= RESSOURC_APA * 12]
  dfri_1[, n_part:=1 + 1*(couple_rfs == "1")]

  dfri_1[, carte_inva := 0]

  dfri_1[, ressource_apa := RESSOURC_APA]
  dfri_1[, RESSOURC_APA := NULL]

  print("Premier calage")
  dfri_1 <- calage_cadrage_dom(dfri_1)

  dfri_1 <- data.frame(dfri_1)
  dfri_1 <- ponderation_dep(dfri_1, "plan_conso")
  dfri_1 <- data.table(dfri_1)

  #sauvegarde
  print("Ecriture plan_noti")
  fwrite(dfri_1, file=paste(chemin_imputation_, "data_ri2017_imputations_plan_noti.csv",sep='/'))

  #sous consommation et valeur manquantes-------------------

  #départements supprimés car trop de NA
  dfri_2 <- dfri_1[!(DEP %in% c("002","013","017","054","059","066","082","091"))]
  dfri_2[,un:=1]
  #528 349

  #On crée des variables de correction
  dfri_2[,DECHUM_APA_COR:=DECHUM_APA]
  dfri_2[,DECNONHUM_APA_COR:=DECNONHUM_APA]
  dfri_2[,DECHPB_APA_COR:=DECHPB_APA]

  #Si DECNONHUM_APA est négatif on le met en manquant
  dfri_2[DECNONHUM_APA_COR<0, DECNONHUM_APA_COR:=NA]

  #Si il n'y a pas de recours notifié à l'aide et que le montant payé est manquant, on le met à zéro
  dfri_2[AIDEHUM==0 & is.na(DECHUM_APA_COR), DECHUM_APA_COR:=0]
  dfri_2[AUTREAIDE==0 & AIDEPONC==0 & AIDEAID==0 & AIDEACC==0 & AIDEHEB==0 & is.na(DECNONHUM_APA_COR), DECNONHUM_APA_COR:=0]

  #Si le montant total n'est pas manquant, qu'un montant payé est à zéro. On impute à l'autre montant payé (s'il est manquant) le montant total
  dfri_2[is.na(DECHUM_APA_COR) & DECNONHUM_APA_COR==0 & !is.na(DECHPB_APA_COR), DECHUM_APA_COR:=DECHPB_APA_COR]
  dfri_2[DECHUM_APA_COR==0 & is.na(DECNONHUM_APA_COR) & !is.na(DECHPB_APA_COR), DECNONHUM_APA_COR:=DECHPB_APA_COR]

  #Si le montant total est manquant, mais que les montants des deux autres ne sont pas manquant, le total est la somme des deux
  dfri_2[is.na(DECHPB_APA_COR) & !is.na(DECHUM_APA_COR) & !is.na(DECNONHUM_APA_COR), DECHPB_APA_COR:=DECHUM_APA_COR+DECNONHUM_APA_COR]

  #Si le montant total n'est pas manquant, qu'un montant payé n'est pas manquant et est inférieur au montant total, on impute à l'autre montant q'il est manquant la différence entre les deux
  dfri_2[AIDEHUM==1 & is.na(DECHUM_APA_COR) & !is.na(DECNONHUM_APA_COR) & !is.na(DECHPB_APA_COR) & DECNONHUM_APA_COR<DECHPB_APA_COR, DECHUM_APA_COR:=DECHPB_APA_COR-DECNONHUM_APA_COR]
  dfri_2[(AUTREAIDE==1 | AIDEPONC==1 | AIDEAID==1 | AIDEACC==1 | AIDEHEB==1) & is.na(DECNONHUM_APA_COR) & !is.na(DECHUM_APA_COR) & !is.na(DECHPB_APA_COR) & DECHUM_APA_COR<DECHPB_APA_COR, DECNONHUM_APA_COR:=DECHPB_APA_COR-DECHUM_APA_COR]

  #Si un montant payé est supérieur ou égal au plafond de l'APA 2017 + 10, on met ce montant en manquant.
  dfri_2[is.na(DECHUM_APA_COR) & !is.na(DECNONHUM_APA_COR) & DECNONHUM_APA_COR>=PLAFOND_2017, DECNONHUM_APA_COR:=NA]
  # -> pas beaucoup d'observation + différence max à 27, on ne modifie pas.
  # -> pas beaucoup d'observation + différence max à 62, on ne modifie pas (?)

  #Si un montant payé est supérieur ou égal au montant total alors qu'il y a du recours à l'autre aide (dont le montant est manquant), on met le montant total en valeur manquante
  dfri_2[AIDEHUM==1 & is.na(DECHUM_APA_COR) & !is.na(DECNONHUM_APA_COR) & !is.na(DECHPB_APA_COR) & DECNONHUM_APA_COR>=DECHPB_APA_COR, DECHPB_APA_COR:=NA]
  dfri_2[(AUTREAIDE==1|AIDEPONC==1 | AIDEAID==1 | AIDEACC==1 | AIDEHEB==1) & is.na(DECNONHUM_APA_COR) & !is.na(DECHUM_APA_COR) & !is.na(DECHPB_APA_COR) & DECHUM_APA_COR>=DECHPB_APA_COR,DECHPB_APA_COR:=NA]

  #Si la variable de recours est manquante et que tous les montants sont manquants ou l'autre montant=le montant total, on considère qu'il n'y a pas de recours
  dfri_2[AIDEHUM==9 & is.na(DECHUM_APA_COR) & is.na(DECNONHUM_APA_COR) & is.na(DECHPB_APA_COR), DECHUM_APA_COR:=0]
  dfri_2[AIDEHUM==9 & is.na(DECHUM_APA_COR) & DECNONHUM_APA_COR==DECHPB_APA_COR, DECHUM_APA_COR:=0]
  dfri_2[AIDEHUM==9 & is.na(DECHUM_APA_COR) & DECNONHUM_APA_COR==DECHPB_APA_COR, DECHUM_APA_COR:=0]
  #Pas beaucoup d'individus + pas une grosse différence donc on va garder tel quel et dire qu'il n'y a pas de recours à l'aide humaine
  dfri_2[!((AUTREAIDE==1 | AIDEPONC==1 | AIDEAID==1 | AIDEACC==1 | AIDEHEB==1) | (AUTREAIDE==0 & AIDEPONC==0 & AIDEAID==0 & AIDEACC==0 & AIDEHEB==0))
         & is.na(DECHUM_APA_COR) & is.na(DECNONHUM_APA_COR) & is.na(DECHPB_APA_COR), DECNONHUM_APA_COR:=0]
  dfri_2[!((AUTREAIDE==1 | AIDEPONC==1 | AIDEAID==1 | AIDEACC==1 | AIDEHEB==1) | (AUTREAIDE==0 & AIDEPONC==0 & AIDEAID==0 & AIDEACC==0 & AIDEHEB==0))
         & is.na(DECHUM_APA_COR) & DECNONHUM_APA_COR==DECHPB_APA_COR, DECHUM_APA_COR:=0]

  #Si la variable de recours est manquante et que l'autre montant<le montant total, on impute le montant payé restant par la différence
  dfri_2[AIDEHUM==9 & is.na(DECHUM_APA_COR) & DECNONHUM_APA_COR==DECHPB_APA_COR, DECHUM_APA_COR:=DECHPB_APA_COR-DECNONHUM_APA_COR]

#   summary(dfri_2$DECHUM_APA) #143072 NA
#   summary(dfri_2$DECHUM_APA_COR)#101643 NA

  #cas 1.b, 1.c, 1.e : imputation de DECHUM_APA à partir de DECNONHUM_APA
  model1 <- glm(DECHUM_APA ~ DECNONHUM_APA_COR + ressource_apa +
                          as.character(gir_imput) + SEXE_C + age_plus60 +
                          age_plus60_carre + duree_apad_t + couple_rfs, family = gaussian(link = "log"),
                        data = dfri_2[AIDEHUM==1 & !is.na(DECHUM_APA) & DECHUM_APA!=0 & !is.na(DECNONHUM_APA_COR)], weights = poids_post_imputation)
  #on filtre par !is.na(DECHUM_APA) et pas !is.na(DECHUM_APA_COR) parce qu'on considère que ça reste assez d'individus pour faire de la prédiction
  # et on ne veut pas modéliser à partir des 0 imputé lorsque le recours est nul.

  df_p1a <- dfri_2[AIDEHUM==1 & is.na(DECHUM_APA_COR) & !is.na(DECNONHUM_APA_COR) & DECNONHUM_APA_COR>=DECHPB_APA_COR] #0 obs
  df_p1b <- dfri_2[AIDEHUM==1 & is.na(DECHUM_APA_COR) & !is.na(DECNONHUM_APA_COR) & is.na(DECHPB_APA_COR)] #81081 obs
  df_p1 <- rbind(df_p1a,df_p1b)
  ID_dfp1 <- df_p1$ID
  dfri_cor <- dfri_2[!(ID %in% ID_dfp1)]
  df_p1$DECHUM_APA_COR <- predict(model1, newdata = df_p1, type='response')
  df_p1$DECHPB_APA_COR <- df_p1$DECHUM_APA_COR + df_p1$DECNONHUM_APA_COR #toutes les obs de df_p1 ont is.na(DECHPB_APA)
  rm(model1)

  #cas 1.d
  model2 <- glm((DECHUM_APA/DECHPB_APA_COR) ~ DECHPB_APA_COR + ressource_apa +
                          as.character(gir_imput) + SEXE_C + age_plus60 +
                          age_plus60_carre + duree_apad_t + couple_rfs, family = binomial(link = "logit"),
                        data = dfri_2[AIDEHUM==1 & !is.na(DECHUM_APA) & (DECHUM_APA/DECHPB_APA_COR)>=0 & (DECHUM_APA/DECHPB_APA_COR)<=1 & !is.na(DECHPB_APA_COR)],
                        weights = poids_post_imputation)

  df_p2 <- dfri_cor[AIDEHUM==1 & is.na(DECHUM_APA_COR) & is.na(DECNONHUM_APA_COR) & !is.na(DECHPB_APA_COR)] #9008 obs
  ID_dfp2 <- df_p2$ID
  dfri_cor <- dfri_cor[!(ID %in% ID_dfp2)]
  df_p2$ratio_hum_hpb <- predict(model2, newdata = df_p2, type='response')
  rm(model2)

  df_p2$DECHUM_APA_COR <- df_p2$ratio_hum_hpb * df_p2$DECHPB_APA_COR

  df_p2$DECNONHUM_APA_COR <- df_p2$DECHPB_APA_COR - df_p2$DECHUM_APA_COR
  df_p2$ratio_hum_hpb <- NULL

  #cas 1.f
  model3 <- glm(DECHUM_APA ~ ressource_apa +
                          as.character(gir_imput) + SEXE_C + age_plus60 +
                          age_plus60_carre + duree_apad_t + couple_rfs, family = gaussian(link = "log"),
                        data = df_m3 <- dfri_2[AIDEHUM==1 & !is.na(DECHUM_APA) & DECHUM_APA!=0], weights = poids_post_imputation)
  model3c <- glm(DECNONHUM_APA ~ ressource_apa +
                           as.character(gir_imput) + SEXE_C + age_plus60 +
                           age_plus60_carre + duree_apad_t + couple_rfs, family = gaussian(link = "log"),
                         data = df_m3b <- dfri_2[AIDEHUM==1 & !is.na(DECNONHUM_APA) & DECNONHUM_APA>0], weights = poids_post_imputation)

  df_p3 <- dfri_cor[AIDEHUM==1 & is.na(DECHUM_APA_COR) & is.na(DECNONHUM_APA_COR) & is.na(DECHPB_APA_COR)] #4002 obs
  ID_dfp3 <- df_p3$ID
  dfri_cor <- dfri_cor[!(ID %in% ID_dfp3)]
  df_p3$DECHUM_APA_COR <- predict(model3, newdata = df_p3, type='response')
  rm(model3)

  df_p3$DECNONHUM_APA_COR <- predict(model3c, newdata = df_p3, type='response')
  rm(model3c)

  df_p3[,DECHPB_APA_COR:=DECHUM_APA_COR+DECNONHUM_APA_COR]

  model4b <- glm(DECHPB_APA ~ ressource_apa +
                           as.character(gir_imput) + SEXE_C + age_plus60 +
                           age_plus60_carre + duree_apad_t + couple_rfs, family = gaussian(link = "log"),
                         data = dfri_2[AIDEHUM==9 & !is.na(DECHPB_APA) & DECHPB_APA!=0], weights = poids_post_imputation)
  #14214 obs
  model4c <- glm(DECNONHUM_APA ~ ressource_apa +
                           as.character(gir_imput) + SEXE_C + age_plus60 +
                           age_plus60_carre + duree_apad_t + couple_rfs, family = gaussian(link = "log"),
                         data = dfri_2[AIDEHUM==9 & !is.na(DECNONHUM_APA) & DECNONHUM_APA!=0], weights = poids_post_imputation)

  df_p4 <- dfri_cor[AIDEHUM==9 & is.na(DECHUM_APA_COR)] #9062 obs
  ID_dfp4 <- df_p4$ID
  dfri_cor <- dfri_cor[!(ID %in% ID_dfp4)]

  df_p4b <- df_p4[AIDEHUM==9 & is.na(DECHUM_APA_COR) & is.na(DECHPB_APA_COR)] #1268 obs
  df_p4b$DECHPB_APA_COR <- predict(model4b, type='response', newdata=df_p4b)
  rm(model4b)
  df_p4b[!is.na(DECNONHUM_APA_COR),DECHUM_APA_COR:=DECHPB_APA_COR-DECNONHUM_APA_COR] #1268

  df_p4b[DECHUM_APA_COR<0, DECHPB_APA_COR:=DECNONHUM_APA_COR]
  df_p4b[DECHUM_APA_COR<0, DECHUM_APA_COR:=0]


  df_p4c <- df_p4[AIDEHUM==9 & is.na(DECHUM_APA_COR) & is.na(DECNONHUM_APA_COR)] #4863 obs
  df_p4c$DECNONHUM_APA_COR <- predict(model4c, newdata = df_p4c, type='response')
  rm(model4c)

  df_p4c[!is.na(DECHPB_APA_COR), DECHUM_APA_COR:=DECHPB_APA_COR-DECNONHUM_APA_COR]
  df_p4c[DECHUM_APA_COR<0,DECNONHUM_APA_COR:=DECHPB_APA_COR]
  df_p4c[DECHUM_APA_COR<0,DECHUM_APA_COR:=0]

  ID_dfp4b <- df_p4b$ID
  ID_dfp4c <- df_p4c$ID
  df_p4 <- df_p4[!(ID %in% ID_dfp4b) & !(ID %in% ID_dfp4c)]#9062 -> 2931
  df_p4 <- rbind(df_p4, df_p4b, df_p4c)#9062
  rm(df_p4b, df_p4c)

  dfri_2 <- data.table(rbind(dfri_cor, df_p1, df_p2, df_p3, df_p4))
  rm(dfri_cor, df_p1, df_p2, df_p3, df_p4)

  # summary(dfri_2$DECHUM_APA) #143072 NA
  # summary(dfri_2$DECHUM_APA_COR)#2931 NA
  # #histogram(dfri_2$DECHUM_APA_COR)

  #cas 2.b,
  model5 <- glm(DECNONHUM_APA ~ DECHUM_APA_COR + ressource_apa +
                          as.character(gir_imput) + SEXE_C + age_plus60 +
                          age_plus60_carre + duree_apad_t + couple_rfs, family = gaussian(link = "log"),
                        data = df_m5 <- dfri_2[(dfri_2$AUTREAIDE==1 | dfri_2$AIDEPONC==1 | dfri_2$AIDEAID==1 | dfri_2$AIDEACC==1 | dfri_2$AIDEHEB==1)
                                                          & !is.na(DECNONHUM_APA) & DECNONHUM_APA>0 & !is.na(DECHUM_APA_COR)], weights = poids_post_imputation)
  #on filtre par !is.na(DECHUM_APA) et pas !is.na(DECHUM_APA_COR) parce qu'on considère que ça reste assez d'individus pour faire de la prédiction
  # et on ne veut pas modéliser à partir des 0 imputé lorsque le recours est nul.
  # histogram(log(df_m1$DECNONHUM_APA))

  df_p5a <- dfri_2[(dfri_2$AUTREAIDE==1 | dfri_2$AIDEPONC==1 | dfri_2$AIDEAID==1 | dfri_2$AIDEACC==1 | dfri_2$AIDEHEB==1)
                              & is.na(DECNONHUM_APA_COR) & !is.na(DECHUM_APA_COR) & DECHUM_APA_COR>=DECHPB_APA_COR] #28906 obs
  df_p5b <- dfri_2[(dfri_2$AUTREAIDE==1 | dfri_2$AIDEPONC==1 | dfri_2$AIDEAID==1 | dfri_2$AIDEACC==1 | dfri_2$AIDEHEB==1)
                              & is.na(DECNONHUM_APA_COR) & !is.na(DECHUM_APA_COR) & is.na(DECHPB_APA_COR)] #63 obs
  df_p5 <- rbind(df_p5a,df_p5b)
  ID_dfp5 <- df_p5$ID
  dfri_cor <- dfri_2[(!(ID %in% ID_dfp5))]
  df_p5$DECNONHUM_APA_COR <- predict(model5, newdata = df_p5, type='response')

  df_p5$DECHPB_APA_COR <- df_p5$DECHUM_APA_COR + df_p5$DECNONHUM_APA_COR

  #cas 2.d -> 0 observations
  model6 <- glm(DECNONHUM_APA ~ DECHPB_APA_COR + ressource_apa +
                          as.character(gir_imput) + SEXE_C + age_plus60 +
                          age_plus60_carre + duree_apad_t + couple_rfs, family = gaussian(link = "log"),
                        data = df_m6 <- dfri_2[(AUTREAIDE==1 | AIDEPONC==1 | AIDEAID==1 | AIDEACC==1 | AIDEHEB==1)
                                                          & !is.na(DECNONHUM_APA) & DECNONHUM_APA>0 & !is.na(DECHPB_APA_COR)],
                        weights = poids_post_imputation)

  df_p6 <- dfri_cor[(AUTREAIDE==1 | AIDEPONC==1 | AIDEAID==1 | AIDEACC==1 | AIDEHEB==1)
                               & is.na(DECNONHUM_APA_COR) & is.na(DECHUM_APA_COR) & !is.na(DECHPB_APA_COR)] #0 obs

  #cas 2 recours manquants
  model7 <- glm(DECNONHUM_APA ~ ressource_apa +
                          as.character(gir_imput) + SEXE_C + age_plus60 +
                          age_plus60_carre + duree_apad_t + couple_rfs, family = gaussian(link = "log"),
                        data = df_m7 <- dfri_2[!((AUTREAIDE==1 | AIDEPONC==1 | AIDEAID==1 | AIDEACC==1 | AIDEHEB==1) | (AUTREAIDE==0 & AIDEPONC==0 & AIDEAID==0 & AIDEACC==0 & AIDEHEB==0))
                                                          & !is.na(DECNONHUM_APA) & DECNONHUM_APA>0], weights = poids_post_imputation)

  model7b <- glm(DECHPB_APA ~ ressource_apa +
                           as.character(gir_imput) + SEXE_C + age_plus60 +
                           age_plus60_carre + duree_apad_t + couple_rfs, family = gaussian(link = "log"),
                         data = df_m7b <- dfri_2 %>% filter(!((AUTREAIDE==1 | AIDEPONC==1 | AIDEAID==1 | AIDEACC==1 | AIDEHEB==1) | (AUTREAIDE==0 & AIDEPONC==0 & AIDEAID==0 & AIDEACC==0 & AIDEHEB==0))
                                                            & !is.na(DECHPB_APA) & DECHPB_APA>0), weights = poids_post_imputation)

  df_p7 <- dfri_cor[!((AUTREAIDE==1 | AIDEPONC==1 | AIDEAID==1 | AIDEACC==1 | AIDEHEB==1) | (AUTREAIDE==0 & AIDEPONC==0 & AIDEAID==0 & AIDEACC==0 & AIDEHEB==0))
                               & is.na(DECNONHUM_APA_COR)] #9177 obs
  ID_dfp7 <- df_p7$ID
  dfri_cor <- dfri_cor[!(ID %in% ID_dfp7)]
  df_p7$DECNONHUM_APA_COR <- predict(model7, type='response', newdata = df_p7)

  df_p7[is.na(DECHPB_APA_COR) & !is.na(DECHUM_APA_COR), DECHPB_APA_COR:=DECHUM_APA_COR+DECNONHUM_APA_COR]
  df_p7[is.na(DECHUM_APA_COR), DECHUM_APA_COR:=DECHPB_APA_COR-DECNONHUM_APA_COR]
  df_p7[DECHUM_APA_COR<0, DECHUM_APA_COR:=0]

  rm(dfri_1) #pour faire de la place dans R sinon c'est trop lourd

  dfri_2 <- data.table(rbind(dfri_cor, df_p5, df_p7)) #528349
  rm(dfri_cor, df_p5, df_p7)

  dfri_2[,AIDEHUM_planpaye:=ifelse(is.na(DECHUM_APA_COR),"9",ifelse(DECHUM_APA_COR==0,"0","1"))]
  dfri_2[,AIDENONHUM_planpaye:=ifelse(is.na(DECNONHUM_APA_COR),"9",ifelse(DECNONHUM_APA_COR==0,"0","1"))]

  # Taux de participation empirique
  dfri_2[!is.na(NOTPB) & !(is.na(plan_2017_non_imput)), TP_emp := NOTPB/plan_2017_non_imput] #422 valeurs au dessus de 1, 17673 NA
  dfri_2[TP_emp > 1, TP_emp := NA][TP_emp > 0.9, TP_emp := 0.9] # on borne à 90%, la participation maximale

  # Plan consommé à partir du montant effectivement payé par le CD et du taux de participation au montant notifié
  dfri_2[!is.na(DECHPB_APA) & !is.na(TP_emp), plan_conso_2017_noncor := DECHPB_APA/(1-TP_emp)]
  dfri_2[!is.na(DECHPB_APA_COR) & !is.na(TP_emp), plan_conso_2017 := DECHPB_APA_COR/(1-TP_emp)]

  # On borne le consommé corrigé par le montant notifié
  dfri_2[, plan_conso_2017 := pmin(plan_noti_2017, plan_conso_2017)]

  dfri_2[, ratio_conso_plan_noncor := plan_conso_2017_noncor/plan_noti_2017]
  dfri_2[, ratio_conso_plan := plan_conso_2017/plan_noti_2017]

  # summary(dfri_2$ratio_conso_plan_noncor)
  # # Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's
  # # 0.000   0.640   0.925   0.767   1.000   1.000   27612
  #
  # summary(dfri_2$ratio_conso_plan)
  # # Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's
  # # 0.000   0.749   0.973   0.845   1.000   1.000   18095

  # Sous consommation conventionnelle : quand le consommé est < à 95% du notifié
  dfri_2[ratio_conso_plan_noncor >=0.95, ss_conso_noncor := 0][ratio_conso_plan_noncor < 0.95, ss_conso_noncor := 1]
  dfri_2[ratio_conso_plan >=0.95, ss_conso := 0][ratio_conso_plan < 0.95, ss_conso := 1]

  freq(dfri_2$ss_conso_noncor,UseNA="always",plot=F)
  freq(dfri_2$ss_conso,UseNA="always",plot=F)

  dfri_2[, montant_ss_conso_noncor := plan_noti_2017-plan_conso_2017_noncor]
  dfri_2[, montant_ss_conso := plan_noti_2017-plan_conso_2017]

  # Montant consommé, pareil mais uniquement pour l'aide humaine
  dfri_2[, plan_conso_2017_ah_noncor := DECHUM_APA/(1-TP_emp)]
  dfri_2[, plan_conso_2017_ah := DECHUM_APA_COR/(1-TP_emp)]

  # On borne le montant consommé en aide humaine par le montant notifié en aide humaine
  dfri_2[, plan_conso_2017_ah := pmin(plan_conso_2017_ah, AIDEHUM_NOT)]
  dfri_2[, ratio_conso_plan_ah_noncor := plan_conso_2017_ah_noncor/AIDEHUM_NOT]
  dfri_2[, ratio_conso_plan_ah := plan_conso_2017_ah/AIDEHUM_NOT]

  dfri_2[ratio_conso_plan_ah_noncor >=0.95, ss_conso_ah_noncor := 0][ratio_conso_plan_ah_noncor < 0.95, ss_conso_ah_noncor := 1]
  dfri_2[ratio_conso_plan_ah >=0.95, ss_conso_ah := 0][ratio_conso_plan_ah < 0.95, ss_conso_ah := 1]

  dfri_2[, montant_ss_conso_ah_noncor := AIDEHUM_NOT-plan_conso_2017_ah_noncor]
  dfri_2[, montant_ss_conso_ah := AIDEHUM_NOT-plan_conso_2017_ah]

  # modalité de ref (la plus fréquente) : femme seule
  dfri_2[, sexe_couple_mod := case_when(
    SEXE_C==1 &  couple==0 ~ "homme_seul",
    SEXE_C==2 &  couple==0 ~ "1.femme_seule",
    SEXE_C==1 &  couple==1 ~ "homme_en_couple",
    SEXE_C==2 &  couple==1 ~ "femme_en_couple",
    TRUE ~ NA_character_
  )]

  dfri_2[, TR_AGER_mod := case_when(
    AGE_C >= 60 & AGE_C < 65 ~ "1.60-65 ANS",
    AGE_C >= 65 & AGE_C < 70 ~ "2.65-69 ans",
    AGE_C >= 70 & AGE_C < 75 ~ "3.70-74 ans",
    AGE_C >= 75 & AGE_C < 80 ~ "4.75-79 ans",
    AGE_C >= 80 & AGE_C < 85 ~ "5.80-84 ans",
    AGE_C >= 85 & AGE_C < 90 ~ "6.85-89 ans",
    AGE_C >= 90 ~ "0.90 ans +",
  )]


  dfri_2[, gir_imput_mod := case_when(
    gir_imput==1 ~ "gir 1",
    gir_imput==2 ~ "gir 2",
    gir_imput==3 ~ "gir 3",
    gir_imput==4 ~ "0.gir 4"
  )]

  dfri_2[, TR_ressource_apa_mod := case_when(
    ressource_apa < 803 ~ "1. moins de 803",
    ressource_apa >= 803 & ressource_apa < 1000 ~ "2.[803-1000)",
    ressource_apa >= 1000 & ressource_apa < 1200 ~ "3.[1000-1200)",
    ressource_apa >= 1200 & ressource_apa < 1400 ~ "0.[1200-1400)",
    ressource_apa >= 1400 & ressource_apa < 1600 ~ "5.[1400-1600)",
    ressource_apa >= 1600 & ressource_apa < 1800 ~ "6.[1600-1800)",
    ressource_apa >= 1800 & ressource_apa < 2000 ~ "7.[1800-2000)",
    ressource_apa >= 2000 & ressource_apa < 2500 ~ "8.[2000-2500)",
    ressource_apa >= 2500 ~ "9.plus de 2500"
  )]

  dfri_2[, DEP_mod  := case_when(DEP == "062" ~ ".062", TRUE ~ DEP)]

  set.seed(234)
  model <- tobit(ratio_conso_plan ~
                   TR_AGER_mod + sexe_couple_mod+ TR_ressource_apa_mod  +plan_noti_2017
                 +gir_imput_mod +DEP_mod,
                 dist ="gaussian",
                 left= 0,
                 right = 1,
                 data = dfri_2,
                 na.action = "na.exclude")

  fitt <- predict(model, newdata = dfri_2)
  dfri_2[, ratio_conso_plan_imput := pmin(fitt, 1)]
  dfri_2[, plan_conso_2017_imput := ratio_conso_plan_imput * plan_noti_2017]

  dfri_2[, TR_ressource_apa_mod := NULL]
  dfri_2[, gir_imput_mod := NULL]
  dfri_2[, TR_AGER_mod := NULL]
  dfri_2[, sexe_couple_mod := NULL]
  dfri_2[, DEP_mod := NULL]

  setnames(dfri_2, "plan_conso_2017", "plan_conso_2017_non_imput")
  # Création colonne finale : valeur non imputée si corrigé non manquant, sinon valeur imputée
  dfri_2[, plan_conso_2017 := plan_conso_2017_non_imput][is.na(plan_conso_2017_non_imput), plan_conso_2017 := plan_conso_2017_imput]

  dfri_2 <- calage_cadrage_dom(dfri_2)
  dfri_2[, pond_dep:=NULL]
  dfri_2 <- data.frame(dfri_2)
  dfri_2 <- ponderation_dep(dfri_2, "plan_conso")
  dfri_2 <- data.table(dfri_2)
  print("Ecriture plan_conso")

  fwrite(dfri_2, file=paste(chemin_imputation_, "data_ri2017_imputations_plan_conso.csv",sep='/'))

  df1 <- composition_plan(dfri_2)
  df1b <- construction_indic_mode_emploi(df1)
  df2 <- ajout_traitement_nb_heures(df1b)
  set.seed(12)
  df3 <- deplafonnement_plan_2017(df2)
  df3 <- calage_cadrage_dom(df3)

  df3 <- data.frame(df3)
  df3 <- ponderation_dep(df3, "plan_conso_avec_compo")
  df3 <- data.table(df3)

  print("Ecriture plan_conso_avec_compo")
  fwrite(df3, file=paste(chemin_imputation_, "data_ri2017_imputations_plan_conso_avec_compo.csv",sep='/'))


  return("Imputations terminées")
}


