Les utilisateurs de ce modèle sont invités à signaler à la DREES leurs travaux issus de sa réutilisation, ainsi que les éventuels problèmes qu'ils y rencontreraient, en écrivant à DREES_CODE@sante.gouv.fr.

Pour les utlisateurs de Gitlab, il est possible de le faire en ouvrant une "issue" dans le projet.

Si vous souhaitez contribuer au modèle, il est également possible de suggérer des ajouts ou modifications via une branche dédiée et une "merge request".
